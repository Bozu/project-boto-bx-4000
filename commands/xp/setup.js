const Discord = require('discord.js');

module.exports = {
	name: 'setup',
	description: 'Setup du serveur pour l\'xp',
	serverOnly: false,
	repertory: 'commands/xp',
	args: false,
	execute(message, args) {
      // We use "request" module
      var request = require("request");

      // We search the PixabayKey and build the URL
      var keys = require('../../utils/apiKeys.json');
      var url = "https://pixabay.com/api/?key=" + keys.PixabayKey + "&q=fox";

      // Then we request a json response from the site
      request({
          url: url,
          json: true
      }, function (error, response, body) {

          // If it responded correctly
          if (!error && response.statusCode === 200) {
              // Build the RichEmbedmbed
              let random = Math.floor(Math.random() * (body.hits.length));
              let foxEmbed = new Discord.RichEmbed()
              .setAuthor("Pixabay.com", "https://pixabay.com/static/img/logo_square.png", "https://pixabay.com/")
              .setColor("#FFAA00")
              .setImage(body.hits[random].largeImageURL)
              .setFooter("Image posted by " + body.hits[random].user, body.hits[random].userImageURL);

               return message.channel.send(foxEmbed);
          }
          else return message.channel.send("`❌ Something went wrong...``")
      })
	},
};
