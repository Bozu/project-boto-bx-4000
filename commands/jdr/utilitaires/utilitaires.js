module.exports = {

	jdrJetsCmd: function(N, Val) {
		const mesNb = new Array();
		for (let i = 1; i < parseInt(N, 10) + 1; i++) {
			mesNb.push(parseInt(Math.floor(Math.random() * Math.floor(Val)) + 1));
		}
		return mesNb;
	},

	getPerso: function(id) {
		switch (id) {
		case '294917236634943490': return 'Aldwyn';
		case '234623513216942081': return 'Thyasth';
		case '234674134334963712': return 'Diego';
		case '96275409724768256': return 'Hipuko';
		case '356170211256172547': return 'Tarwena';
		case '298155084397805578': return 'Seya';
		case '211251631113764864': return 'Ottman';
		default: return '';
		}
	},

	getJoueur: function(nom) {
		switch (nom) {
		case 'Aldwyn': return '294917236634943490';
		case 'Thyasth': return '234623513216942081';
		case 'Diego': return '234674134334963712';
		case 'Hipuko': return '96275409724768256';
		case 'Tarwena': return '356170211256172547';
		case 'Seya': return '298155084397805578';
		case 'Ottman': return '211251631113764864';
		default: return '192933855110365184';
		}
	},
};