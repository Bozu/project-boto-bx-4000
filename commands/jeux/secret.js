const fs = require('fs');
const Discord = require('discord.js');
const client = new Discord.Client();

const tabSecret = ['President', 'Chancelier'];
const tabSecret2 = ['Election', 'Identite', 'Mort', 'Speciale'];
const tabSecret3 = ['Vote', 'Veto'];

const tabLettre = ['A) ', 'B) ', 'C) ', 'D) ', 'E) ', 'F) ', 'G) ', 'H) ', 'I) ', 'J) '];
const tabNb = ['🇦', '🇧', '🇨', '🇩', '🇪', '🇫', '🇬', '🇭', '🇮', '🇯'];
const tabChoix = ['✅', '❎'];
const tabEmoTile = ['🔵', '🔴', '⚫'];

const tabRole = [-1,0,1,1,1,1,0,1,0,1];

let tabTile = [];
let tabTileLegis = [];
let playerKill = -1;

let enJeu = false;

let players = [];
let playersDead = [];
let selecChancelier = [];

let tileLib;
let tileFas;

let president;
let chancelier;
let ancienPresident = -1;
let ancienChancelier = -1;
let spePresident = -1;
let speChancelier = -1;
let spe = -1;

let votePour;
let voteContre;

let fail = 0;
let cardRej = -1;
let veto = false;

module.exports = {
	name: 'secret',
	description: 'Lance une partie de secret hitler !',
	args: true,
	execute(message, args) {
		message.delete();

		if(enJeu) {
			message.channel.send('Une partie est déjà en cours');
			return;
		}

		players = [];
		tileLib = 0;
		tileFas = 0;

		message.mentions.users.forEach(user => {
			players.push([user, -2]);
		});

		if (players.length < 3 || players.length > 10) {
			message.channel.send('Vous n\'êtes pas assez (ou trop) de joueurs !');
			return;
		}

		enJeu = true;

		tabTile = randomizeTile();
		attribRoles();

		president = Math.floor(Math.random() * players.length)-1;
		attribPresident();
	},
};

function attribRoles() {
	let tabNum = [];
	let num;
	let numHit;

	for (let i = 0; i < players.length; i++) {
		do{
			num = Math.floor(Math.random() * players.length);
		} while(tabNum.includes(num));
		players[i][1] = tabRole[num];
		tabNum.push(num);
	}

	for (let i = 0; i < players.length; i++) {
		players[i][0].send('===== Début de partie =====');
		if (players[i][1] == -1) {
			players[i][0].send('```Votre rôle : Hitler\nVotre but est de faire passer 6 lois fascistes tout en vous faisant passer pour un libéral.```');
			numHit = i;
		}
		else if (players[i][1] == 0) {
			players[i][0].send('```Votre rôle : fasciste\nVotre but est de faire passer 6 lois fascistes tout en vous faisant passer pour un libéral.```');
		}
		else
			players[i][0].send('```Votre rôle : liberal\nVotre but est de faire passer 5 lois libérales.```');
	}

	//Pour chaque joueur
	players.forEach(play => {
		//Si le joueur est un fasciste ou si c'est Hitler et qu'il y a 6 joueurs ou moins
		if(play[1] == 0 || (players.length <= 6 && play[1] == -1)){
			//Pour chaque joueurBis
			players.forEach(play2 => {
				//Si le joueurBis est un fasciste et qu'il est different de joueur
				if(play2[1] == 0 && play != play2)
					//Joueur sait que joueurBis est fasciste
					play[0].send('```--> ' + play2[0].username + ' est un fasciste.```');
			});
			//Si joueur est un fasciste et qu'il y a plus de 6 joueurs
			if(play[1] == 0)
				//Joueur sait qui est Hitler
				play[0].send('```--> ' + players[numHit][0].username + ' est Hitler.```');
		}
	});
}

function attribPresident(){
	let ques = '';
	selecChancelier = [];

	if(spe == 1){
		president = spePresident;
		ancienPresident = spePresident;
		ancienChancelier = speChancelier;
		if(playerKill > spePresident)
			spe = -1;
	}
	else if(spe == 0)
		spe++;
	else if(spe == -1)
		president = (president + 1) % players.length;

	if(spe == 1)
		spe = -1;

	if(tabTile.length < 3)
		tabTile = randomizeTile();

	for(let i = 0; i < players.length; i++){
		if(players.length <= 5){
			if(i != ancienChancelier && i != president)
				selecChancelier.push([players[i][0], players[i][1]]);
		}
		else{
			if(i != ancienChancelier && i != ancienPresident && i != president)
				selecChancelier.push([players[i][0], players[i][1]]);
		}
	}

	/*switch(selecChancelier.length){
		case 10:
			ques = '\nJ) ' + selecChancelier[9][0].username + ques;
		case 9:
			ques = '\nI) ' + selecChancelier[8][0].username + ques;
		case 8:
			ques = '\nH) ' + selecChancelier[7][0].username + ques;
		case 7:
			ques = '\nG) ' + selecChancelier[6][0].username + ques;
		case 6:
			ques = '\nF) ' + selecChancelier[5][0].username + ques;
		case 5:
			ques = '\nE) ' + selecChancelier[4][0].username + ques;
		case 4:
			ques = '\nD) ' + selecChancelier[3][0].username + ques;
		case 3:
			ques = '\nC) ' + selecChancelier[2][0].username + ques;
		case 2:
			ques = '\nB) ' + selecChancelier[1][0].username + ques;
		case 1:
			ques = 'A) ' + selecChancelier[0][0].username + ques;
	}*/

	for(let i = 0; i < selecChancelier.length; i++){
		ques += tabLettre[i] + selecChancelier[i][0].username + "\n";
	}

	for(let i = 0; i < players.length; i++){
		players[i][0].send('=== Election du chancelier ===');
		if(i == president){
			players[i][0].send('```Vous êtes le président.```');
			players[i][0].send({ embed: {
				color: 0x00AE86,
				title: 'Election - Vous êtes le président. Vous devez élire un chancelier.',
				fields: [{
					name: 'Qui voulez-vous voir chancelier ?',
					value: ques,
				}]
			}})
			.then(async function(msg) {
				await msg.react('🇦');
				if(selecChancelier.length > 1)
					await msg.react('🇧');
				if(selecChancelier.length > 2)
					await msg.react('🇨');
				if(selecChancelier.length > 3)
					await msg.react('🇩');
				if(selecChancelier.length > 4)
					await msg.react('🇪');
				if(selecChancelier.length > 5)
					await msg.react('🇫');
				if(selecChancelier.length > 6)
					await msg.react('🇬');
				if(selecChancelier.length > 7)
					await msg.react('🇭');
				if(selecChancelier.length > 8)
					await msg.react('🇮');
				if(selecChancelier.length > 9)
					await msg.react('🇯');

				const filter = (reaction, user) => {
					return tabNb.includes(reaction.emoji.name) && user.id === players[i][0].id;
				};

				msg.awaitReactions(filter, { max: 1 })
				.then(collected => {
					const reaction = collected.first();

					let rep = 0;
					while(tabNb[rep] != reaction.emoji.name)
						rep++;

					election(rep);
				});
			});
		}
		else
			players[i][0].send('```' + players[president][0].username + ' est le président. Il va élir un chancelier.```');
	}

	if(playersDead.length > 0){
		playersDead.forEach(play => {
			play[0].send('=== Election du chancelier ===');
			play[0].send('```' + players[president][0].username + ' est le président. Il va élir un chancelier.```');
		});
	}
}

function election(rep){
	votePour = 0;
	voteContre = 0;
	for(let i = 0; i < players.length; i++){
		if(players[i][0].id == selecChancelier[rep][0].id)
			chancelier = i;
	}
	players.forEach(play => {
		play[0].send('```' + players[president][0].username + ' veut élir ' + players[chancelier][0].username + ' comme chancelier.```');
		play[0].send({ embed: {
			color: 0x00AE86,
			title: 'Vote - Etes-vous pour ou contre l\'élection de ' + players[chancelier][0].username + ' comme chancelier ?',
			fields: [{
				name: 'Vote',
				value: 'Pour : ✅\nContre : ❎',
			}]
		}})
		.then(async function(msg) {
			await msg.react('✅');
			await msg.react('❎');

			const filter = (reaction, user) => {
				return tabChoix.includes(reaction.emoji.name) && user.id === play[0].id;
			};

			msg.awaitReactions(filter, { max: 1 })
			.then(collected => {
				const reaction = collected.first();

				let rep = 0;
				while(tabChoix[rep] != reaction.emoji.name)
					rep++;

				aVote(rep);
			});
		});
	});

	if(playersDead.length > 0){
		playersDead.forEach(play => {
			play[0].send('```' + players[president][0].username + ' veut élir ' + players[chancelier][0].username + ' comme chancelier.```');
		});
	}
}

function aVote(rep){

	if(rep == 0)
		votePour++;
	else
		voteContre++;

	if((votePour + voteContre) == players.length){
		if(votePour > voteContre)
			legislationPresident();
		else
			electionFail();
	}
}

function electionFail(){
	fail++;
	if(fail == 3){

		fail = 0;
		let nomLoi;
		let topTile = tabTile.pop();

		if(topTile == 0){
			tileLib++;
			nomLoi = 'libérale';
		}
		else{
			tileFas++;
			nomLoi = 'fasciste';
		}

		let ami;
		players.forEach(play => {
			if((play[1] != topTile) && (play[1] == 1 || topTile == 1))
				ami = '+';
			else
				ami = '-';

			play[0].send('```diff\n' + ami + ' Trois elections ont échoué, ce qui a fait passé une loi ' + nomLoi + '.```');
		});

		if(playersDead.length > 0){
			playersDead.forEach(play => {
				if((play[1] != topTile) && (play[1] == 1 || topTile == 1))
					ami = '+';
				else
					ami = '-';

				play[0].send('```diff\n' + ami + ' Trois elections ont échoué, ce qui a fait passé une loi ' + nomLoi + '.```');
			});
		}



		ancienChancelier = -1;
		ancienPresident = -1;
		chancelier = -1;

		card(topTile);
	}
	else{
		chancelier = -1;

		attribPresident();
	}
}

function legislationPresident(){

	fail = 0;

	if(tileFas >= 3 && players[chancelier][1] == -1){
		finGame(true, false);
		return;
	}

	for(let i = 0; i < players.length; i++){
		if(i == chancelier)
			players[chancelier][0].send('```Vous avez été élu chancelier.```');
		else
			players[i][0].send('```' + players[chancelier][0].username + ' a été élu chancelier.```');
	}

	players.forEach(play => {
		play[0].send('=== Phase législative ===');
		if(play[0] != players[president][0])
			play[0].send('```Le président choisi une loi parmi trois à rejeter.```');
	});

	if(playersDead.length > 0){
		playersDead.forEach(play => {
			play[0].send('```' + players[chancelier][0].username + ' a été élu chancelier.```');
			play[0].send('=== Phase législative ===');
			play[0].send('```Le président choisi une loi parmi trois à rejeter.```');
		});
	}

	let nbLib = 0;
	let nbFas = 0;
	let value = '';
	tabTileLegis = [];

	for(let i = 0; i < 3; i++){
		let topTileX = tabTile.pop();
		tabTileLegis.push(topTileX);
		if(topTileX == 0){
			nbLib++;
			value = 'Loi libérale : 🔵' + value;
		}
		else{
			nbFas++;
			value = 'Loi fasciste : 🔴' + value;
		}
		if(i < 2)
			value = '\n' + value;
	}

	players[president][0].send({ embed: {
		color: 0x00AE86,
		title: 'President - Quelle loi voulez-vous rejeter ?',
		fields: [{
			name: 'Voici les 3 lois reçues.',
			value: value,
		}]
	}})
	.then(async function(msg) {
		if(nbLib > 0)
			await msg.react('🔵');
		if(nbFas > 0)
			await msg.react('🔴');

		const filter = (reaction, user) => {
			return tabEmoTile.includes(reaction.emoji.name) && user.id === players[president][0].id;
		};

		msg.awaitReactions(filter, { max: 1 })
		.then(collected => {
			const reaction = collected.first();

			let rep = 0;
			while(tabEmoTile[rep] != reaction.emoji.name)
				rep++;

			legislationChancelier(rep);
		});
	});
}

function legislationChancelier(rep){

	players.forEach(play => {
		if(play[0] != players[chancelier][0])
			play[0].send('```Le chancelier choisi une loi parmi deux à mettre en place.```');
	});

	if(playersDead.length > 0){
		playersDead.forEach(play => {
			play[0].send('```Le chancelier choisi une loi parmi deux à mettre en place.```');
		});
	}

	let nbLib = 0;
	let nbFas = 0;
	let value = '';
	let dejaVu = false;

	cardRej = rep;

	for(let i = 0; i < 3; i++){
		if(!(tabTileLegis[i] == rep && !dejaVu)){
			if(tabTileLegis[i] == 0){
				nbLib++;
				value = 'Loi libérale : 🔵\n' + value;
			}
			else{
				nbFas++;
				value = 'Loi fasciste : 🔴\n' + value;
			}
		}
		else
			dejaVu = true;
	}

	players[chancelier][0].send({ embed: {
		color: 0x00AE86,
		title: 'Chancelier - Quelle loi voulez-vous mettre en place ?',
		fields: [{
			name: 'Voici les 2 lois reçues.',
			value: value,
		}]
	}})
	.then(async function(msg) {
		if(nbLib > 0)
			await msg.react('🔵');
		if(nbFas > 0)
			await msg.react('🔴');
		if(tileFas == 5 && !veto)
			await msg.react('⚫');
		if(veto)
			veto = false;

		const filter = (reaction, user) => {
			return tabEmoTile.includes(reaction.emoji.name) && user.id === players[chancelier][0].id;
		};

		msg.awaitReactions(filter, { max: 1 })
		.then(collected => {
			const reaction = collected.first();

			let rep = 0;
			while(tabEmoTile[rep] != reaction.emoji.name)
				rep++;

			legislationFin(rep);
		});
	});
}

function legislationFin(rep){

	if(rep == 0){
		tileLib++;
		nomLoi2 = 'libérale';
	}
	else{
		tileFas++;
		nomLoi2 = 'fasciste';
	}

	let ami;
	players.forEach(play => {
		if((play[1] != rep) && (play[1] == 1 || rep == 1))
			ami = '+';
		else
			ami = '-';
		play[0].send('```diff\n' + ami + ' Le président et le chancelier se sont mis d\'accord pour faire passer une loi ' + nomLoi2 + '.```');
	});

	if(playersDead.length > 0){
		playersDead.forEach(play => {
			if((play[1] != rep) && (play[1] == 1 || rep == 1))
				ami = '+';
			else
				ami = '-';
			play[0].send('```diff\n' + ami + ' Le président et le chancelier se sont mis d\'accord pour faire passer une loi ' + nomLoi2 + '.```');
		});
	}

	ancienPresident = president;
	ancienChancelier = chancelier;
	chancelier = -1;
	card(rep);
}

function card(typeCard){
	if(tileLib == 5 ||tileFas == 6){
		finGame(false, false);
		return;
	}

	power(typeCard);
}

function power(typeCard){
	if(typeCard != 1){
		attribPresident();
		return;
	}

	switch(tileFas){
		case 5:
			kill(false, -1);
			break;
		case 4:
			kill(false, -1);
			break;
		case 3:
			if(players.length > 6){
				presidentSpecial(false, -1);
			}
			else{
				watchCards();
			}
			break;
		case 2:
			if(players.length > 6)
				watchPoliticCard(false, 0);
			else
				attribPresident();
			break;
		case 1:
			if(players.length > 8)
				watchPoliticCard(false, 0);
			else
				attribPresident();
			break;
		default:
			attribPresident();
			break;
	}
}

function watchCards(){

	let tabTileBis = [];
	let value = '';

	for(let i = 1; i < 4; i++){
		if(tabTile[tabTile.length - i] == 0)
			value = 'Loi libérale : 🔵\n' + value;
		else
			value = 'Loi fasciste : 🔴\n' + value;
	}

	players[president][0].send({ embed: {
		color: 0x00AE86,
		title: 'Lois - Vous avez le pouvoir de connaître les trois prochaines lois tirées.',
		fields: [{
			name: 'Voici les trois prochaines lois.',
			value: value,
		}]
	}});

	attribPresident();
}

function watchPoliticCard(choice, numPlayer){

	if(!choice){
		title = 'Identite - Vous avez le pouvoir de connaître le partie politique d\'une personne.';
		name = 'De qui voulez-vous connaître l\identité ?';
		choix(title, name);
	}
	else{
		let party;
		let ami;
		if (players[numPlayer][1] == 1)
			party = 'libéral';
		else
			party = 'fasciste';

		if((players[president][1] != players[numPlayer][1]) && (players[president][1] == 1 || players[numPlayer][1] == 1))
			ami = '-';
		else
			ami = '+';

		players[president][0].send('```diff\n'  + ami + ' ' + players[numPlayer][0].username + ' est un ' + party + '.```');

		attribPresident();
	}
}

function presidentSpecial(choice, numPlayer){

	if(!choice){
		spePresident = president;
		speChancelier = ancienChancelier;
		spe++;
		ancienPresident = -1;
		ancienChancelier = -1;

		title = 'Speciale - Vous avez le pouvoir de décidé du prochain président.';
		name = 'Qui voulez-vous voir président ?';
		choix(title, name);
	}
	else{
		president = numPlayer;
		attribPresident();
	}
}

function kill(choice, numPlayer){

	if(!choice){
		let title = 'Mort - Vous avez le pouvoir de tuer une personne.';
		let name = 'Qui voulez-vous tuer ?';
		choix(title, name);
	}
	else{
		let party;
		let ami;
		if (players[numPlayer][1] == -1){
			finGame(false, true);
			return;
		}

		playerKill = numPlayer;

		players.forEach(play => {
			if(play[0] == players[numPlayer][0])
				play[0].send('```fix\nLe président ' + players[president][0].username + ' a décidé de vous tuer.```');
			else if(play[0] == players[president][0])
				play[0].send('```fix\nVous avez tué ' + players[numPlayer][0].username + '.```');
			else
				play[0].send('```fix\n' + players[numPlayer][0].username + ' a été tué par le président ' + players[president][0].username + '.```');
		});

		if(playersDead.length > 0){
			playersDead.forEach(play => {
				play[0].send('```fix\n' + players[numPlayer][0].username + ' a été tué par le président ' + players[president][0].username + '.```');
			});
		}

		let newPlayers = [];
		players.forEach(play => {
			if(play[0] != players[numPlayer][0])
				newPlayers.push([play[0], play[1]]);
			else
				playersDead.push([play[0], play[1]]);
		})

		players = newPlayers;

		attribPresident();
	}
}

function demandeVeto(pres, reponse){

	if(!pres){
		players.forEach(play => {
			play[0].send('```Demande de veto de la part de ' + players[chancelier][0].username + '.```');
		});

		if(playersDead.length > 0){
			playersDead.forEach(play => {
				play[0].send('```Demande de veto de la part de ' + players[chancelier][0].username + '.```');
			});
		}

		players[president][0].send({ embed: {
			color: 0x00AE86,
			title: 'Veto - Etes-vous d\'accord avec le chancelier pour rejetez toutes les lois de ce tour ?',
			fields: [{
				name: 'Vote',
				value: 'Pour : ✅\nContre : ❎',

			}]
		}})
		.then(async function(msg) {
			await msg.react('✅');
			await msg.react('❎');

			const filter = (reaction, user) => {
				return tabChoix.includes(reaction.emoji.name) && user.id === players[president][0].id;
			};

			msg.awaitReactions(filter, { max: 1 })
			.then(collected => {
				const reaction = collected.first();

				let rep = 0;
				while(tabChoix[rep] != reaction.emoji.name)
					rep++;

				demandeVeto(true, rep);
			});
		});
	}
	else{
		if(reponse == 0){
			players.forEach(play => {
				play[0].send('```Demande de veto acceptée par le président.```');
			});

			if(playersDead.length > 0){
				playersDead.forEach(play => {
					play[0].send('```Demande de veto acceptée par le président.```');
				});
			}

			veto = true;
			attribPresident();
		}
		else
			legislationChancelier(cardRej);
	}
}

function choix(title, name){

	let ques = '';

	/*switch(players.length){
		case 10:
			if(players[9][0] != players[president][0])
				ques = '\nH) ' + players[9][0].username + ques;
		case 9:
			if(players[8][0] != players[president][0])
				ques = '\nI) ' + players[8][0].username + ques;
		case 8:
			if(players[7][0] != players[president][0])
				ques = '\nH) ' + players[7][0].username + ques;
		case 7:
			if(players[6][0] != players[president][0])
				ques = '\nG) ' + players[6][0].username + ques;
		case 6:
			if(players[5][0] != players[president][0])
				ques = '\nF) ' + players[5][0].username + ques;
		case 5:
			if(players[4][0] != players[president][0])
				ques = '\nE) ' + players[4][0].username + ques;
		case 4:
			if(players[3][0] != players[president][0])
				ques = '\nD) ' + players[3][0].username + ques;
		case 3:
			if(players[2][0] != players[president][0])
				ques = '\nC) ' + players[2][0].username + ques;
		case 2:
			if(players[1][0] != players[president][0])
				ques = '\nB) ' + players[1][0].username + ques;
		case 1:
			if(players[0][0] != players[president][0])
				ques = 'A) ' + players[0][0].username + ques;
	}*/

	for(let j = 0; j < players.length; j++){
		if(players[j][0] != players[president][0])
			ques += tabLettre[j] + players[j][0].username + "\n";
	}

	players[president][0].send({ embed: {
		color: 0x00AE86,
		title: title,
		fields: [{
			name: name,
			value: ques,
		}]
	}})
	.then(async function(msg) {
		if (players[0][0] != players[president][0])
			await msg.react('🇦');
		if(players.length > 1 && players[1][0] != players[president][0])
			await msg.react('🇧');
		if(players.length > 2 && players[2][0] != players[president][0])
			await msg.react('🇨');
		if(players.length > 3 && players[3][0] != players[president][0])
			await msg.react('🇩');
		if(players.length > 4 && players[4][0] != players[president][0])
			await msg.react('🇪');
		if(players.length > 5 && players[5][0] != players[president][0])
			await msg.react('🇫');
		if(players.length > 6 && players[6][0] != players[president][0])
			await msg.react('🇬');
		if(players.length > 7 && players[7][0] != players[president][0])
			await msg.react('🇭');
		if(players.length > 8 && players[8][0] != players[president][0])
			await msg.react('🇮');
		if(players.length > 9 && players[9][0] != players[president][0])
			await msg.react('🇯');

		const filter = (reaction, user) => {
			return tabNb.includes(reaction.emoji.name) && user.id === players[president][0].id;
		};

		msg.awaitReactions(filter, { max: 1 })
		.then(collected => {
			const reaction = collected.first();

			let rep = 0;
			while(tabNb[rep] != reaction.emoji.name)
				rep++;

			const spe = title.trim().split(/ +/g).shift();

			if(spe == 'Identite')
				watchPoliticCard(true, rep);
			else if(spe == 'Mort')
				kill(true, rep);
			else if(spe == 'Speciale')
				presidentSpecial(true, rep);
		});
	});
}

function finGame(hitChanc, hitDead){
	let text;
	if(hitChanc){
		players.forEach(play => {
			if(play[1] == 1)
				text = '-';
			else
				text = '+';
			play[0].send('```diff\n' + text + ' Hitler a été élu chancelier, victoire des fascistes.```');
		});

		if(playersDead.length > 0){
			playersDead.forEach(play => {
				if(play[1] == 1)
					text = '-';
				else
					text = '+';
				play[0].send('```diff\n' + text + ' Hitler a été élu chancelier, victoire des fascistes.```');
			});
		}
	}
	else if(hitDead){
		players.forEach(play => {
			if(play[1] == 1)
				text = '+';
			else
				text = '-';
			play[0].send('```diff\n' + text + ' Hitler est mort, victoire des libéraux.```');
		});

		if(playersDead.length > 0){
			playersDead.forEach(play => {
				if(play[1] == 1)
					text = '+';
				else
					text = '-';
				play[0].send('```diff\n' + text + ' Hitler est mort, victoire des libéraux.```');
			});
		}
	}
	else if(tileLib == 5){
		players.forEach(play => {
			if(play[1] == 1)
				text = '+';
			else
				text = '-';
			play[0].send('```diff\n' + text + ' Cinq lois libérales ont été passé, victoire des libéraux.```');
		});

		if(playersDead.length > 0){
			playersDead.forEach(play => {
				if(play[1] == 1)
					text = '+';
				else
					text = '-';
				play[0].send('```diff\n' + text + ' Cinq lois libérales ont été passé, victoire des libéraux.```');
			});
		}
	}
	else if(tileFas == 6){
		players.forEach(play => {
			if(play[1] == 1)
				text = '-';
			else
				text = '+';
			play[0].send('```diff\n' + text + ' Six lois fascistes ont été passé, victoire des fascistes.```');
		});

		if(playersDead.length > 0){
			playersDead.forEach(play => {
				if(play[1] == 1)
					text = '-';
				else
					text = '+';
				play[0].send('```diff\n' + text + ' Six lois fascistes ont été passé, victoire des fascistes.```');
			});
		}
	}

	tabTile = [];
	tabTileLegis = [];
	playerKill = -1;
	enJeu = false;
	players = [];
	selecChancelier = [];
	tileLib = 0;
	tileFas = 0;
	president = -1;
	chancelier = -1;
	ancienPresident = -1;
	ancienChancelier = -1;
	spePresident = -1;
	speChancelier = -1;
	spe = -1;
	votePour = -1;
	voteContre = -1;
	fail = 0;
	cardRej = -1;
	veto = false;
}

function randomizeTile(){
	//6 liberal
	//11 fascist
	let tabRand = [];
	let tabUse = [];

	let rand;

	for(let i = 0; i < 17; i++){
		do{
			rand = Math.floor(Math.random() * 17);
		} while(tabUse.includes(rand));

		if(rand > 5){
			tabRand.push(1);
		}
		else{
			tabRand.push(0);
		}

		tabUse.push(rand);
	}

	players.forEach(play => {
		play[0].send('```Les cartes ont été mélangées.```');
	});

	if(playersDead.length > 0){
		playersDead.forEach(play => {
			play[0].send('```Les cartes ont été mélangées.```');
		});
	}

	return tabRand;
}

function help(message){

	message.delete();

	message.author.send({ embed: {
		"author": {
			"name": "Le maître du jeu",
			"icon_url": "https://pbs.twimg.com/profile_images/913788896038707200/wA-eysAh_400x400.jpg"
		},
		"thumbnail": {
			"url": "https://i.ytimg.com/vi/h9D4SEftgBs/maxresdefault.jpg"
		},
		"title": "Règles du jeu de Secret Hitler",
		"description": "Secret Hitler est un jeu à rôles cachés confrontant les libéraux et les fascistes. Le but des libéraux est de faire passer 5 lois libérales ou de tuer Hitler, et celui des fascistes est de faire passer 6 lois fascistes ou de promouvoir Hitler chancelier.",
		"color": 13055768,
		"fields": [
			{
				"name": "Phase d'élection",
				"value": "Le président va élir un chancelier. Celui-ci ne pouvant pas être le président, ni l'ancien chancelier (ni l'ancien président, s'il y a un certain nombre de joueurs). Lorsque le chancelier est choisit par le président, le peuple vote pour ou contre l'élection de ce chancelier. S'il a une majorité de pour, le chancelier est élu et on passe à la phase législative. Sinon, le chancelier n'est pas élu et le président passe le relais à un autre. Et on recommence l'élection. Après trois échecs d'élection, le chaos s'installe et une loi passe, qu'elle soit libérale ou fasciste.\n\n"
			},
			{
				"name": "Phase législative",
				"value": "Pendant la phase législative, le président et le chancelier collaborent pour faire passer une loi. Le président reçoit trois lois et en fait passer deux au chancelier. Ce dernier rejettera une loi et fera passer l'autre. Et puis on recommence depuis la phase d'élection avec un nouveau président.\n\n"
			},
			{
				"name": "Pouvoirs",
				"value": "Si une loi fasciste passe, le président actuel reçoit un pouvoir (s'il y a peu de joueurs, il n'en reçoit qu'à partir d'un certain nombre de lois fascistes passées). Il y a cinq pouvoirs : Fidélité, Coup d'Oeil, Election Speciale, Exection, Veto.\n\n",
			},
			{
				"name": "-> Fidélité",
				"value": "Le président peut connaître le parti\nd'un joueur.",
				"inline": true
			},
			{
				"name": "-> Coup d'Oeil",
				"value": "Le président peut connaître\nles trois prochaines lois.",
				"inline": true
			},
			{
				"name": "-> Election spéciale",
				"value": "Le président désigne n'importe qui\ncomme prochain président.",
				"inline": true
			},
			{
				"name": "-> Execution",
				"value": "Le président peut tuer un\njoueur.",
				"inline": true
			},
			{
				"name": "-> Veto",
				"value": "Pendant les prochaines phases législatives, le chancelier peut demander au président de refuser les deux lois reçus. Si le président accepte, les lois sont rejettées, sinon, le chancelier doit en choisir une.",
				"inline": true
			},
			{
				"name": "\n\nLes lois",
				"value": "Il y a 17 lois au total. 6 lois libérales et 11 lois fascistes. Elles sont mélangées aléatoirement. Quand il ne reste plus que deux cartes ou moins, toutes les lois sont de nouveau mélangées."
			},
			{
				"name": "5-6 joueurs",
				"value": "- 1 Hitler\n- 1 fascistes\n- 3-4 libéraux.\nTous les fascistes savent avec qui ils sont.\nOrdre de pouvoirs :\nRien - Rien - Coup d'Oeil - Execution - Execution+Veto"
			},
			{
				"name": "7-8 joueurs",
				"value": "- 1 Hitler\n- 2 fascistes\n- 4-5 libéraux.\nTous les fascistes (sauf Hitler) savent avec qui ils sont et qui est Hitler.\nOrdre de pouvoirs :\nRien - Fidélité - Election spéciale - Execution - Execution+Veto"
			},
			{
				"name": "9-10 joueurs",
				"value": "- 1 Hitler\n- 3 fascistes\n- 5-6 libéraux.\nTous les fascistes (sauf Hitler) savent avec qui ils sont et qui est Hitler.\nOrdre de pouvoirs :\nFidélité - Fidélité - Election spéciale - Execution - Execution+Veto"
			}
		]
	}});
}
/*
client.on("messageReactionAdd", (reaction, user) => {
	if(user.bot) return;

	let rep = 0;

	if(reaction.message.channel == user.dmChannel){

		if(reaction.count != 2) return;

		reaction.message.delete();

		const args = reaction.message.embeds[0].title.trim().split(/ +/g);
		const command = args.shift();

		if(tabSecret2.includes(command)){
			switch(reaction.emoji.name) {
				case '🇦':
					rep = 0;
					break;
				case '🇧':
					rep = 1;
					break;
				case '🇨':
					rep = 2;
					break;
				case '🇩':
					rep = 3;
					break;
				case '🇪':
					rep = 4;
					break;
				case '🇫':
					rep = 5;
					break;
				case '🇬':
					rep = 6;
					break;
				case '🇭':
					rep = 7;
					break;
				case '🇮':
					rep = 8;
					break;
				case '🇯':
					rep = 9;
					break;
			}
			if(command == 'Election')
				election(rep);
			else if(command == 'Identite')
				watchPoliticCard(true, rep);
			else if(command == 'Mort')
				kill(true, rep);
			else if(command == 'Speciale')
				presidentSpecial(true, rep);

		}
		else if(tabSecret3.includes(command)){
			switch(reaction.emoji.name) {
				case '✅':
					rep = 0;
					break;
				case '❎':
					rep = 1;
					break;
			}
			if(command == 'Veto')
				demandeVeto(true, rep);
			else
				aVote(rep);
		}
		else if(tabSecret.includes(command)){
			switch(reaction.emoji.name) {
				case '🔵':
					rep = 0;
					break;
				case '🔴':
					rep = 1;
					break;
				case '⚫':
					demandeVeto(false, rep);
					return;
					break;
			}
			if(command == 'President')
				legislationChancelier(rep);
			else if(command == 'Chancelier')
				legislationFin(rep);
		}
	}
});
*/