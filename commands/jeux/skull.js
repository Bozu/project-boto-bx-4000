const SkullPlayer = require('./skull/skullPlayer.js');
const Discord = require('discord.js');

let turn = 1;
let bet = 0;
let counter = 0;
let gameStarted = false;
let players = [];
let roundPlayer;
const tabReaction = ['🇦', '🇧', '🇨', '🇩', '🇪', '🇫', '🇬', '🇭', '🇮', '🇯', '🇰', '🇱'];


module.exports = {
	name:'skull',
	description: 'Lance une partie de skull',
	args: true,
	repertory: 'commands/jeux',
	execute(message, args) {
		if (gameStarted) return message.channel.send('Une partie est déjà en cours');
		const userList = [];
		message.mentions.users.map(user => {
			userList.push(user);
		});
		if (userList.length != args.length) return message.channel.send('Fais pas le con Billy, ne passe QUE des utilisateurs en paramètre (et je veux pas les voir deux fois !)');
		if (userList.length < 3) return message.channel.send('Trop peu de joueurs');
		if (userList.some((e) => {
			return e.bot;
		})) return message.channel.send('Fdp trouve toi des amis au lieu de vouloir jouer avec des bots.');
		initGame(userList);
		startRound(message);
	},
};

function initGame(userList) {
	players = [];
	turn = 1;
	counter = 0;
	bet = 0;
	userList.forEach((user) => {
		players.push(new SkullPlayer(user));
	});
	gameStarted = true;
	chooseRandPlay();
}

function stackEmbed() {
	const playEmbed = new Discord.RichEmbed()
		.setColor('#5F427A')
		.setTitle(`Tour de ${roundPlayer.getName()}`);

	players.forEach((player) => {
		playEmbed.addField(`${player.name} C: ${player.cards.length} P: ${player.points}`, player.getStackText(), true);
	});
	return playEmbed;
}

function startRound(message) {

	players.forEach((player) => {
		message.client.fetchUser(player.id)
			.then((user) => {
				user.send(`\`\`\` Début du Round ${turn} \`\`\``);
			});
	});
	bet = 0;
	counter = 0;
	choice(message);
}

function startBetPhase(message) {
	players.forEach((player) => {
		message.client.fetchUser(player.id)
			.then((user) => {
				user.send(`\`\`\` ${roundPlayer.getName()} veut faire une mise \`\`\``);
				user.send(`\`\`\` Début de la phase de paris ${turn} \`\`\``);
			});
	});

	betting(message);
}

function startRevealPhase(message) {
	players.forEach((player) => {
		message.client.fetchUser(player.id)
			.then((user) => {
				user.send('```Début de la phase de révélation```');
			});
	});
	reveal(message);
}

function choice(message) {
	let possibility = '';
	const playEmbed = stackEmbed();

	players.forEach((player) => {
		message.client.fetchUser(player.id)
			.then((user) => {
				user.send(playEmbed);
			});
	});

	if (roundPlayer.isFlowerBet()) possibility += 'Poser une fleur : 💮\n';
	if (roundPlayer.isSkullBet()) possibility += 'Poser le crâne : 💀\n';
	if (isBetPossible()) possibility += 'Faire un pari : ♠\n';

	const embd = new Discord.RichEmbed()
		.setTitle('C\'est votre tour !')
		.addField('Votre main :', roundPlayer.getCardInHand())
		.addField('Que voulez vous faire ?', possibility);
	message.client.fetchUser(roundPlayer.id)
		.then((usr) => {
			usr.send(embd)
				.then((botMess) => {
					if (roundPlayer.isFlowerBet()) botMess.react('💮');
					if (roundPlayer.isSkullBet()) botMess.react('💀');
					if (isBetPossible()) botMess.react('♠');

					const filter = (reaction, user) => {
						return ['💮', '💀', '♠'].includes(reaction.emoji.name) && user.id === roundPlayer.id;
					};

					botMess.awaitReactions(filter, { max: 1 })
						.then(collected => {
							const reaction = collected.first();
							if (reaction.emoji.name === '💮') {
								roundPlayer.stackCard(1);
								nextPlayer();
								choice(message);
							}
							else if (reaction.emoji.name === '💀') {
								roundPlayer.stackCard(0);
								nextPlayer();
								choice(message);
							}
							else {
								startBetPhase(message);
							}
						});
				});
		});
}

function betting(message) {
	const playEmbed = stackEmbed();
	players.forEach((player) => {
		message.client.fetchUser(player.id)
			.then((user) => {
				user.send(playEmbed);
				if (bet != 0) user.send(`\`\`\`La mise actuelle est de ${bet}\`\`\``);
			});
	});
	const embd = new Discord.RichEmbed()
		.setTitle('C\'est votre tour !')
		.addField('Si vous voulez miser :', 'Tappez "bet [nombre]" (en chiffre)\nps : si le nombre est trop grand, on considèrera que c\'est le nombre max de cartes');

	if (bet != 0) {
		embd.addField('Si vous voulez vous coucher :', 'Tappez "pass"');
	}
	message.client.fetchUser(roundPlayer.id)
		.then((usr) => {
			usr.send(embd)
				.then((botMess) => {
					const filter = response => {
						let res = false;
						const tabResp = response.content.split(/ +/);
						if (tabResp[0].toLowerCase() === 'bet'
						&& parseInt(tabResp[1]) > 0 && parseInt(tabResp[1]) <= getTotalStack() && parseInt(tabResp[1]) > bet) {
							res = true;
						}
						else if (tabResp[0].toLowerCase() === 'pass' && bet != 0) {
							res = true;
						}
						return res;
					};

					botMess.channel.awaitMessages(filter, { max: 1 })
						.then(collected => {
							const tabCollect = collected.first().content.split(/ +/);
							if (tabCollect[0].toLowerCase() === 'pass') {
								roundPlayer.pass();
								const sendPass = `\`\`\`${roundPlayer.getName()} a décidé de se coucher.\`\`\``;
								players.forEach((player) => {
									message.client.fetchUser(player.id)
										.then((user) => {
											if (collected.first().author.id !== user.id) user.send(sendPass);
										});
								});

								nextPlayerBet(message);
							}
							else if (tabCollect[0].toLowerCase() === 'bet') {
								bet = parseInt(tabCollect[1]);
								const sendBet = `\`\`\`${roundPlayer.getName()} a décidé de miser ${bet}.\`\`\``;
								players.forEach((player) => {
									message.client.fetchUser(player.id)
										.then((user) => {
											if (collected.first().author.id !== user.id) user.send(sendBet);
										});
								});

								nextPlayerBet(message);

							}
						});
				});
		});

}

function reveal(message) {
	const playEmbed = stackEmbed();
	players.forEach((player) => {
		message.client.fetchUser(player.id)
			.then((user) => {
				user.send(playEmbed);
			});
	});

	if (bet != counter) {

		const embd = new Discord.RichEmbed()
			.setTitle('Révélation')
			.addField('Réagissez avec l\'emoji correspondant', '(ils sont en dessous du message... si vous êtes autiste)');

		if (roundPlayer.stack.length > 0) {
			embd.addField('Votre pile n\'a pas complètement été révélé', `Réagissez avec ${tabReaction[players.indexOf(roundPlayer)]}`);

			message.client.fetchUser(roundPlayer.id)
				.then((user) => {
					user.send(embd)
						.then(botMess => {
							botMess.react(tabReaction[players.indexOf(roundPlayer)]);
							const filter = (reaction, usr) => {
								return reaction.emoji.name === tabReaction[players.indexOf(roundPlayer)] && usr.id === roundPlayer.id;
							};
							botMess.awaitReactions(filter, { max: 1 })
								.then(() => {
									const topCard = roundPlayer.stack.pop();
									if (topCard == 1) {
										counter++;
										roundPlayer.revealedCards.push(topCard);
										reveal(message);
									}
									else {
										roundPlayer.revealedCards.push(topCard);
										removeOwnCard(message);
									}
								});
						});
				});
		}
		else {
			let choices = '';
			const tabEmoji = [];
			for (let i = 0; i < players.length; i++) {
				if (players[i].id != roundPlayer.id && players[i].stack.length != 0) {
					tabEmoji.push(tabReaction[i]);
				}
			}
			for (let i = 0; i < tabEmoji.length; i++) {
				choices += `${tabReaction[tabReaction.indexOf(tabEmoji[i])]} : ${players[tabReaction.indexOf(tabEmoji[i])].name}\n`;
			}
			embd.addField('Chez qui voulez vous retourner une carte ?', choices);

			message.client.fetchUser(roundPlayer.id)
				.then((user) => {
					user.send(embd)
						.then(botMess => {
							for (let i = 0; i < tabEmoji.length; i++) {
								if (players[tabReaction.indexOf(tabEmoji[i])].id != roundPlayer.id) {
									botMess.react(tabEmoji[i]);
								}
							}
							const filter = (reaction, usr) => {
								return tabEmoji.includes(reaction.emoji.name) && usr.id === roundPlayer.id;
							};
							botMess.awaitReactions(filter, { max: 1 })
								.then((collected) => {
									const reaction = collected.first();
									const topCard = players[tabReaction.indexOf(reaction.emoji.name)].stack.pop();
									if (topCard == 1) {
										counter++;
										players[tabReaction.indexOf(reaction.emoji.name)].revealedCards.push(topCard);
										reveal(message);
									}
									else {
										players[tabReaction.indexOf(reaction.emoji.name)].revealedCards.push(topCard);
										removeCard(message, players[tabReaction.indexOf(reaction.emoji.name)]);
									}
								});
						});
				});

		}
	}
	else {
		addPoint(message);
	}
}

function nextPlayer() {
	do {
		roundPlayer = players[(players.indexOf(roundPlayer) + 1) % players.length];
	} while(!roundPlayer.isAlive());

}

function nextPlayerBet(message) {
	let numbOfActiv = 0;
	if (bet == getTotalStack()) {
		players.forEach((player) => {
			message.client.fetchUser(player.id)
				.then((user) => {
					user.send('```Il n\'est plus possible de miser plus```');
				});
		});
		startRevealPhase(message);
	}
	else {
		players.forEach(player => {
			if (player.canBet && player.isAlive()) {
				numbOfActiv++;
				roundPlayer;
			}
		});

		if(numbOfActiv == 1) {

			do {
				roundPlayer = players[(players.indexOf(roundPlayer) + 1) % players.length];
			} while(!roundPlayer.canBet || !roundPlayer.isAlive());

			players.forEach((player) => {
				message.client.fetchUser(player.id)
					.then((user) => {
						user.send('```Il ne reste qu\'un joueur```');
					});
			});
			startRevealPhase(message);
		}
		else {
			do {
				roundPlayer = players[(players.indexOf(roundPlayer) + 1) % players.length];
			} while(!roundPlayer.canBet || !roundPlayer.isAlive());
			betting(message);
		}
	}

}

function addPoint(message) {
	roundPlayer.points++;
	players.forEach((player) => {
		message.client.fetchUser(player.id)
			.then((user) => {
				user.send(`\`\`\`${roundPlayer.name} a gagné un point\`\`\``);
			});
	});
	if (roundPlayer.points == 2) {
		endGame(message);
	}
	else {
		endTurn(message);
	}
}

function endGame(message) {
	players.forEach((player) => {
		message.client.fetchUser(player.id)
			.then((user) => {
				user.send(`\`\`\`Fin de la partie, victoire de ${roundPlayer.name}\`\`\``);
			});
	});
	gameStarted = false;
}

function endTurn(message) {
	const strTour = `\`\`\`Fin du tour ${turn}\`\`\``;
	if (roundPlayer.cards.length == 0) nextPlayer();
	players.forEach((player) => {
		message.client.fetchUser(player.id)
			.then((user) => {
				user.send(strTour);
			});
		player.endTurn();
	});
	turn++;
	startRound(message);
}

function removeOwnCard(message) {
	players.forEach((player) => {
		message.client.fetchUser(player.id)
			.then((user) => {
				if (user.id !== roundPlayer.id) user.send(`\`\`\`Ololz, ${roundPlayer.name} a éssayé de bluffer le con, il va se défausser d'une de ses cartes\`\`\``);
			});
	});

	let main = '';
	roundPlayer.cards.forEach(card => {
		if(card == 1) main += '💮';
		if(card == 0) main += '💀';
	});
	const embd = new Discord.RichEmbed()
		.setTitle('Choisissez une carte à défausser')
		.addField('Votre main', main);

	message.client.fetchUser(roundPlayer.id)
		.then((user) => {
			user.send(embd)
				.then(botMess => {
					if (roundPlayer.cards.includes(1)) botMess.react('💮');
					if (roundPlayer.cards.includes(0)) botMess.react('💀');
					const filter = (reaction, usr) => {
						return ['💮', '💀'].includes(reaction.emoji.name) && usr.id === roundPlayer.id;
					};
					botMess.awaitReactions(filter, { max: 1 })
						.then((collected) => {
							const reaction = collected.first();
							if (reaction.emoji.name === '💮') roundPlayer.removeCardMan(1);
							if (reaction.emoji.name === '💀') roundPlayer.removeCardMan(0);
							endTurn(message);
						});
				});
		});
}

function removeCard(message, removePlayer) {
	players.forEach((player) => {
		message.client.fetchUser(player.id)
			.then((user) => {
				user.send(`\`\`\`Ololz, ${roundPlayer.name} est tombé sur le crâne de ${removePlayer.name}\`\`\``);
			});
	});
	let main = '';
	let i = 0;
	let cardToRemove = '';
	const tabEmoji = [];
	roundPlayer.cards.forEach(() => {
		main += '⚛';
		cardToRemove += `${tabReaction[i]} : Pour supprimer la carte ${i + 1}\n`;
		tabEmoji.push(tabReaction[i]);
		i++;
	});


	const embd = new Discord.RichEmbed()
		.setTitle(`Vous devez choisir une carte de la main de ${roundPlayer.name}, il va devoir la défausser`)
		.addField('Sa "main"', main)
		.addField('Réagissez avec l\'emoji correspondant', cardToRemove);

	message.client.fetchUser(removePlayer.id)
		.then((user) => {
			user.send(embd)
				.then(botMess => {
					tabEmoji.forEach(emoji => {
						botMess.react(emoji);
					});
					const filter = (reaction, usr) => {
						return tabEmoji.includes(reaction.emoji.name) && usr.id === removePlayer.id;
					};
					botMess.awaitReactions(filter, { max: 1 })
						.then((collected) => {
							const reaction = collected.first();
							roundPlayer.removeCard(roundPlayer.shuffleCards()[tabReaction.indexOf(reaction.emoji.name)]);
							endTurn(message);
						});
				});
		});
}


function isBetPossible() {
	let res = true;
	players.forEach((player) => {
		if (player.stack.length == 0 && player.isAlive()) res = false;
	});
	return res;
}

function getTotalStack() {
	let res = 0;
	players.forEach((player) => {
		res += player.stack.length;
	});
	return res;
}

function chooseRandPlay() {
	//	Fonction pour mélanger les cartes (Algorithme de Fisher-Yates)
	let j, x, i;
	const a = Array.from(players);
	for (i = a.length - 1; i > 0; i--) {
		j = Math.floor(Math.random() * (i + 1));
		x = a[i];
		a[i] = a[j];
		a[j] = x;
	}
	roundPlayer = a[0];
}
