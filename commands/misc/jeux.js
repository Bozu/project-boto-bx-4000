const commandsUtil = require('../../utils/commandsUtil.js');

module.exports = {
	name: 'jeux',
	alias: ['jeu', 'game'],
	description: 'Des commandes en rapport avec les différents jeux',
	args: true,
	subCmd: true,
	usage: '[sous commande]',
	execute(message, args) {

		const subCmds =	commandsUtil.getSubCommands('jeux');
		const cmd = subCmds.get(args[0]);
		if (!cmd) return message.reply('Cette sous-commande n\'existe pas');

		try {
			cmd.execute(message, args);
		}
		catch (error) {
			console.error(error);
			message.reply('y a un truc qui a merdé dans cette sous-commande.... oopsies');
		}
	},
};
