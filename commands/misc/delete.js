module.exports = {
	name: 'delete',
	alias: ['suppr', 'del', 'supprimer'],
	description: 'Supprime des messages',
	cooldown: 5,
	serverOnly: true,
	args: true,
	adminOnly: true,
	usage: '[nombre entre 2 et 30]',
	execute(message, args) {
		const nombre = parseInt(args[0]);
		if (isNaN(nombre) || nombre < 2 || nombre > 30) {
			return message.channel.send('L\'utilisation correcte est : `zb.delete [nombre entre 2 et 30]`');
		}

		message.channel.bulkDelete(nombre + 1, true).catch(e => {
			console.error(e);
			message.channel.send('oopsies y a eu une merde quelque part');
		});
	},
};
