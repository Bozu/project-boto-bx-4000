const Discord = require('discord.js');

module.exports = {
	name: 'news',
	description: 'Display some news',
	serverOnly: false,
	args: false,
	execute(message, args) {
      // We use "request" module
      var request = require("request");

      // country of news
      country = 'us';
      if (args.length > 0) country = args[0];

      // We search the NewsKey and build the URL
      var keys = require('../../utils/apiKeys.json');
      var url = 'https://newsapi.org/v2/top-headlines?' +
          'country=' + country + '&' +
          'apiKey=' + keys.NewsKey;

      // Translate generic terms like 'more info'
      let source = 'Source:';
      let publishedBy = 'Published by:';
      let moreInfo = 'More info:';
      let noNews = "No news in this language";
      let trad = "Translation powered by Yandex";
      if (country != 'us') {
         let urlTranslate = 'https://translate.yandex.net/api/v1.5/tr.json/translate' +
                            '?key=' + keys.TranslateKey +
                            '&text=Source:&text=Published%20by:&text=More%20info:&text=No%20news%20in%20this%20language&text=Translation%20powered%20by%20Yandex' +
                            '&lang=en-' + country;

         request({
           url: urlTranslate,
           json: true
         }, function (error, response, body) {
           if (!error && response.statusCode === 200){
             source = body.text[0];
             publishedBy = body.text[1];
             moreInfo = body.text[2];
             noNews = body.text[3];
             trad = body.text[4];
           }
         })
      }

      // Delay a little bit for any translation...
      setTimeout(function() {// Then we request a json response from the site
        request({
            url: url,
            json: true
        }, function (error, response, body) {

            // If it responded correctly
            if (!error && response.statusCode === 200) {
                // Build the RichEmbedmbed
                if (body.articles.length === 0) return message.channel.send("`❌ " + noNews + "...`");
                let random = Math.floor(Math.random() * (body.articles.length));
                let publisher = body.articles[random].author;
                if (publisher === null) publisher = '?';
                let newsEmbed = new Discord.RichEmbed()
                .setAuthor("newsapi.org", "https://images.ecosia.org/8sLimCrBubDT0r1qWxxBZmHApH0=/0x390/smart/https%3A%2F%2Fcdn.pixabay.com%2Fphoto%2F2015%2F02%2F15%2F09%2F33%2Fnews-636978_960_720.jpg", "https://newsapi.org/")
                .setColor("#FFAA00")
                .setTitle(body.articles[random].title)
                .setDescription("__" + source + "__ " + body.articles[random].source.name + "\n ")
                .setImage(body.articles[random].urlToImage)
                .addBlankField()
                .addField(publishedBy + " " + publisher, body.articles[random].description)
                .addBlankField()
                .addField("__" + moreInfo + "__ ", body.articles[random].url)
                .setFooter(trad + ".Translate: http://translate.yandex.com/");


                 return message.channel.send(newsEmbed);
            }
            else return message.channel.send("`❌ Something went wrong...`")
        })
      }, 1500);
	},
};
