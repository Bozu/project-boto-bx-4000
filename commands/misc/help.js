const { prefix } = require('../../config.json');
const Discord = require('discord.js');
const rCheck = require('../../utils/rightcheck.js');

const cmdUtils = require('../../utils/commandsUtil.js');

module.exports = {
	name: 'help',
	description: 'Liste toutes les commandes',
	alias: ['commandes', 'aide'],
	usage: '[command name]',
	serverOnly: false,
	args: false,
	cooldown: 3,
	execute(message, args) {
		const { commands } = message.client;
		const nomBot = message.client.user.username;
		const botAva = message.client.user.displayAvatarURL;

		//	si l'utilisateur utlise help sans arguments, on lui envoie un message en privé avec les commandes
		if(!args.length) {

			let allCommands;

			//	on check si le message a été envoyé en public ou en privé
			if (message.channel.type === 'dm') {
				allCommands = crRichEmbed(message.author, message);
			}
			else {
				allCommands = crRichEmbed(message.member, message);
			}

			//	on envoie le message
			return message.author.send(allCommands)
				.then(() => {

					//	si le message est envoyé en privé, on n'enclenche pas le timer, on envoie directement le message
					if (message.channel.type === 'dm') return;


					message.reply('Je t\'ai envoyé un message avec toutes les commandes. \nRéagissez avec un 👀 pour aussi recevoir le message (vous avez une minute).')
						.then(function(botMess) {
							//	On rajoute une réaction au message
							botMess.react('👀');
							//	On crée un filtre qui s'enclenche si l'emoji est :eyes:
							const filter = (reaction) => {
								return reaction.emoji.name === '👀';
							};

							//	on crée une ReactionCollector prenant en compte le filtre et on lui donne sa durée d'activité en ms.
							const collector = botMess.createReactionCollector(filter, { time: 60000 });
							//	a chaque fois que le collector reçoit une réaction renvoie le help
							collector.on('collect', (reaction) => {

								//	on fait en sorte de ne pas envoyer le message aux bots (le bot a réagit lui aussi au message...)
								if(!reaction.users.last().bot) {

									//	on récupère l'utilisateur ayant réagit sous forme de guildMember
									message.guild.fetchMember(reaction.users.last().id)
										.then(mbr => {
											//	On recrée un embed en fonction de l'utilisateur ayant réagi
											const allCommands2 = crRichEmbed(mbr, message);

											//	On envoie le message au dernier utilisateur de la liste des gens qui ont réagi
											reaction.users.last().send(allCommands2);
										});
								}
							});

							//	à la fin, on indique aux utilisateurs qu'ils ne peuvent plus réagir...
							collector.on('end', () => {
								message.channel.send(`Fin de la minute, réutilisez \`${prefix}help\` pour le recevoir à nouveau`);
							});
						});
				})
				.catch(error => {
					console.error(`Could not send help DM to ${message.author.tag}.\n`, error);
					message.reply('Il y a eu un problème quelque part, peut-être que vous n\'avez pas les messages privés d\'activé');
				});

		}
		//	si l'utilisateur veut des infos sur une commande en particulier
		const name = args[0].toLowerCase();
		const command = commands.get(name) || commands.find(c => c.alias && c.alias.includes(name));

		//	si elle n'existe pas.....
		if(!command) {
			return message.reply('Je ne peux pas t\'aider, cette commande n\'existe pas');
		}

		//	on crée le embed a envoyé
		const cmdEmbed = new Discord.RichEmbed()
			.setColor('#10FFAA')
			.setTitle(`Aide pour la commande ${command.name}`)
			.setAuthor(`${nomBot}`, botAva)
			.setDescription(command.description)
			.setThumbnail(botAva)
			.addBlankField();

		//	on check si la commande possède des attributs particuliers
		if (command.alias) cmdEmbed.addField('Alias :', command.alias.join(', '), true);
		if (command.description) cmdEmbed.addField('Utilisation :', `${prefix}${command.name} ${command.usage}`, true);

		cmdEmbed.addField('Cooldown :', `${command.cooldown || 3} seconde(s)`, true)
			.setTimestamp()
			.setFooter(`${nomBot} by #TeamGDocs`, botAva);
		//	on check si la commande possède des sous commandes
		if (command.subCmd) {
			cmdEmbed.addField('Les sous-commandes :', '-------------------------');
			const subcmds = cmdUtils.getSubCommands(command.name);
			subcmds.array().forEach(subCmd => {
				cmdEmbed.addField(subCmd.name, subCmd.description);
			}
			);
		}
		//	on envoie le message
		message.channel.send(cmdEmbed);
	},
};

function crRichEmbed(member, message) {

	const botAva = message.client.user.displayAvatarURL;
	const nomBot = message.client.user.username;
	//	on crée le embed a envoyé
	const allCommands = new Discord.RichEmbed()
		.setColor('#10FFAA')
		.setTitle('Help')
		.setAuthor(`${nomBot}`, botAva)
		.setDescription('Voici une liste de toutes mes commandes :')
		.setThumbnail(botAva);

	// on vérifie si l'utilisateur a fait la requête en privé ou non
	if (message.channel.type === 'dm') {

		//	on ne peut pas vérifier le rôle de l'utilisateur, on lui envoie les commandes de base
		message.client.commands.array().forEach(cmd => {
			if ((!cmd.repertory && (cmd.status && (!cmd.ownerOnly) && (!cmd.adminOnly)) || rCheck.isOwner(member))) {
				allCommands.addField(cmd.name, cmd.description);
			}
		});
	}
	else {

		//	on vérifie pour chaque commande si l'utilisateur a le droit de l'utiliser et on les rajoutes
		message.client.commands.array().forEach(cmd => {
			if (!cmd.repertory && ((cmd.status && (!cmd.ownerOnly) && (!cmd.adminOnly)) || (rCheck.isAdmin(member) && cmd.status && (!cmd.ownerOnly)) || rCheck.isOwner(member))) {
				allCommands.addField(cmd.name, cmd.description);
			}
		});
	}

	//	on rajoute des infos complémentaires et une petite signature :)
	allCommands.addBlankField()
		.addField('Plus d\'info sur une commande ?', `Utilise ${prefix}help [nom de la commande]`)
		.setTimestamp()
		.setFooter(`${nomBot} by #TeamGDocs`, botAva);

	//	on retourne l'embed
	return allCommands;
}
