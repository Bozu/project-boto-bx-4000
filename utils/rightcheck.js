const { owner, admin } = require('../config.json');

module.exports = {
	isOwner(member) {
		return member.id === owner;
	},
	isAdmin(member) {
		return member.hasPermission('ADMINISTRATOR') || member.roles.some(role => admin.includes(role.name.toLowerCase()));
	},
	isTeamOS(member) {
		return member.roles.some(role => role.name.toLowerCase() === '#teamos');
	},
};
